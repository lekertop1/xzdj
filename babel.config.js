module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    [
      'import',
      {
        libraryName: 'libraryName',
        styleLibraryName: 'src/components'
      }
    ]
  ]
}
