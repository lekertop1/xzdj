source /etc/profile
node -v
npm -v
cnpm -v
cnpm install
node_modules\.bin\vue-cli-service build
cd dist
tar -zcvf dist.tar.gz *
scp dist.tar.gz /var/lib/jenkins/workspace/dcfirecontrol_test-vueManage/intelligent-fire-control-back
cd ../
rm -R dist
cd /var/lib/jenkins/workspace/dcfirecontrol_test-vueManage/
rm -rf dist
mkdir dist
tar -zxvf dist.tar.gz -C dist