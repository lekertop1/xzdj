echo off
set APP_NAME=base-start.jar
setlocal enabledelayedexpansion
set port=9032
echo find run port:  %port% process
for /f "tokens=1-5" %%a in ('netstat -ano ^| find ":%port%"') do (
    if "%%e%" == "" (
        set pid=%%d
        echo The process pid '!pid!' has been released
    ) else (
        set pid=%%e
        echo the process pid for the current port is '!pid!'
    )
    echo !pid!
    taskkill /f /pid !pid!
)
:::pause
echo 5 seconds close this bat
echo Starting program %APP_NAME% -test
java -Xms128m -Xmx512m -server -jar ../target/%APP_NAME% --spring.profiles.active=test
echo run %APP_NAME% Success
pause