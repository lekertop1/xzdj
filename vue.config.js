/*
 * @message: vue配置文件
 */
/**
 * vue配置文件
 */
module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  // 配置代理
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    },
    // 项目启动端口
    port: '7001',
    // 代理
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:6022/',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/'
        }
      }
    }
  },
  configureWebpack: {
    externals: {
      AMap: 'AMap',
      QC: 'QC'
    }
  },
  // 去除预加载，省宽带
  chainWebpack: config => {
    config.plugins.delete('prefetch')
  },
  //  打包地址
  // outputDir: '',
  // 静态资源地址
  publicPath: process.env.NODE_ENV === 'development' ? '/' : '/web'
}