/*
 * @Author: duanyunlong
 * @since: 2020-05-24 09:18:32
 * @lastTime: 2020-07-24 14:01:07
 * @LastAuthor: Do not edit
 * @FilePath: \dc_container\src\utils\oss.js
 * @message:OSS文件上传工具类
 */
'use strict'
/**
 * OSSUtil
 * OSS文件上传工具类
 * 你是一段成熟的代码，要学会自己改bug  ----我是小尾巴
 */

const OSS = require('ali-oss').Wrapper

const param = {
  /**
   * 根据你买的那个区的做相应的更改
   */
  id: 'uploadImage',
  percentage: 0,
  url: '',
  uploadFilesName: '',
  uploadFile: [],
  maxLength: 1,
  limitSuffix: [],
  region: 'oss-cn-hangzhou',
  endpoint: 'oss-cn-hangzhou.aliyuncs.com',
  accessKeyId: 'LTAI9M7M4PhiSjqW',
  accessKeySecret: 'cOYcuk2OPHV5Z2pUn1YZtiMN9Vx2BF',
  bucket: 'qdlsucculent'
}

export default {
  upload (that, files, callback, params) {
    // const urls = []
    // oss 基本配置
    const client = new OSS({
      region: param.region,
      endpoint: param.endpoint,
      accessKeyId: param.accessKeyId,
      accessKeySecret: param.accessKeySecret,
      bucket: param.bucket
    })
    // 初始化进度
    param.percentage = 0
    // 限制上传文件的个数
    // const check = param.uploadfile.length < param.maxLength
    // if (!check) {
    //   this.$Notice.warning({
    //     title: '最多只能上传' + param.maxLength + '张图片。'
    //   })
    //   return false
    // }
    // 遍历判断文件格式
    files.forEach(file => {
      const suffix = file.name.substr(file.name.lastIndexOf('.'))
      if (param.limitSuffix.indexOf(suffix) === -1) {
        return false
      }
    })
    files.forEach(file => {
      client.put('aaa.jpg', file, {
        progress: function * (percentage, cpt) {
          that.percentage = percentage
        }
      }).then((results) => {
        this.uploadfile.push({
          url: results.res.requestUrls[0],
          fileUrl: results.res.requestUrls[0],
          name: results.name
        })
        this.$Message.success('上传成功')
      }).catch((err) => {
        console.log(err)
      })
    })
  }
}
