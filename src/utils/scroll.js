// 禁止滚动
export function stop () {
  // const mo = (e) => {
  //   e.preventDefault()
  // }
  document.body.style.overflow = 'hidden'
  // document.addEventListener('touchmove', mo, { passive: false, capture: true })// 禁止页面滑动
}
// 取消滑动限制
export function move () {
  // const mo = (e) => {
  //   e.preventDefault()
  // }
  document.body.style.overflow = ''// 出现滚动条
  // document.removeEventListener('touchmove', mo, { capture: true })
}
