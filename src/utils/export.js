/*
 * @Author: duanyunlong
 * @since: 2020-05-24 09:18:32
 * @lastTime: 2020-07-24 13:58:07
 * @LastAuthor: Do not edit
 * @FilePath: \dc_container\src\utils\export.js
 * @message: 头部注释
 */
/**
 * 数据导出
 * @param suffix 后缀（不包含协议、ip及端口）
 */
import REQUEST_KEY from '../config/request'

/**
 * 文件导出
 * @param suffix
 * @param fileName
 */
export function exportData (suffix, fileName = '文件导出') {
  const downPath = `${REQUEST_KEY.requestParams.prefix}${suffix}`
  const link = document.createElement('a')
  link.style.display = 'none'
  link.href = downPath
  link.setAttribute('download', fileName)
  document.body.appendChild(link)
  link.click()
}
