// 时间公共方法
import { COUNTRY, COUNTRY_MONTH, COUNTRY_WEEK } from '../config/date'
import { typeOf } from './verification'
import { isSafari } from './browser'
/**
 * 默认工具
 * @type {{}}
 */
const util = {
  /**
   * 数字转字符串
   * @param numberArray
   */
  toString (...numberArray) {
    if (numberArray.length === 0) {
      return ''
    }
    return numberArray.map(number => number < 10 ? '0' + number : number + '')
  }
}
/**
 * 日期时间格式化
 * @param val
 * @param format
 * @param language
 * @param isObj 是否返回对象而非字符串
 */
export function dateFormat (val = new Date(), format = 'yyyy-MM-dd HH:mm', language = '', isObj = false) {
  val = typeOf(val) === 'string' && isSafari() ? val.replace(/-/g, '/') : val
  const date = typeOf(val) === 'date' ? val : new Date(val)
  const Y = date.getFullYear()
  let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1)
  const D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate())
  const h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours())
  const m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes())
  const s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds())
  let e = date.getDay()
  if (COUNTRY.some(item => item === language)) {
    M = COUNTRY_MONTH[language][date.getMonth()]
    e = COUNTRY_WEEK[language][e]
  } else {
    e = COUNTRY_WEEK.CN[e]
  }
  // 验证格式
  let content = format
  content = content.replace(/y+/ig, Y)
  content = content.replace(/M+/g, M)
  content = content.replace(/d+/ig, D)
  content = content.replace(/H+/g, h)
  content = content.replace(/h+/g, h >= 12 ? h - 12 : h)
  content = content.replace(/m+/g, m)
  content = content.replace(/s+/ig, s)
  content = content.replace(/e+/ig, e)
  if (isObj) {
    return {
      date: content,
      timestamp: date.getTime(),
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate(),
      hour: date.getHours(),
      minute: date.getMinutes(),
      second: date.getSeconds(),
      week: date.getDay()
    }
  }
  return content
}

/**
 * 格式化时间显示
 * @param h 详情请参照vue官网说明
 * @param timestamp 时间戳
 * @returns {*}
 */
export function timeText (h, timestamp) {
  return h('Time', {
    props: {
      time: timestamp,
      type: 'datetime',
      interval: 1
    }
  })
}

/**
 * 将日期格式化为yyyy-MM-dd格式
 * @param value
 * @returns {{date, week, month, hour, year, day, timestamp, minute, second}|string|string}
 */
export function formatYYYYMMDD (value = '') {
  if (value == null || value === '') {
    return ''
  } else {
    return dateFormat(value, 'yyyy-MM-dd')
  }
}

/**
 * 格式化时长
 * @param value
 * @param format
 * @param getObject
 * @returns {string|{hour: number, time: string, minute: number, second: number}}
 */
export function formatTime (value = 0, format = 'dd:mm:ss', getObject) {
  const second = value % 60
  value = Number(Math.floor((value /= 60)).toFixed(0))
  const minute = value % 60
  value = Number(Math.floor((value /= 60)).toFixed(0))
  const hour = value
  const [hourStr, minuteStr, secondStr] = util.toString(hour, minute, second)
  let content = format
  if (hour === 0) {
    content = content.replace(/d+:?/ig, '')
  }
  content = content.replace(/d+/ig, hourStr)
  content = content.replace(/m+/ig, minuteStr)
  content = content.replace(/s+/ig, secondStr)
  // 输出对象
  if (getObject) {
    return {
      time: content,
      hour,
      minute,
      second
    }
  }
  return content
}
// 获取从今天开始的后六天的具体年月日，以及星期
export function get7Days () {
  // 获取系统当前时间
  const dateArr = []
  const now = new Date()
  const nowTime = now.getTime()
  const oneDayTime = 24 * 60 * 60 * 1000
  for (let i = 0; i < 7; i++) {
    // 显示周一
    const ShowTime = nowTime + i * oneDayTime
    // 初始化日期时间
    const myDate = new Date(ShowTime)
    const year = myDate.getFullYear()
    let month = myDate.getMonth() + 1
    if (month < 10) {
      month = '0' + month
    }
    let date = myDate.getDate()
    if (date < 10) {
      date = '0' + date
    }
    const arrItem = {
      date: year + '-' + month + '-' + date,
      week: '日一二三四五六'.charAt(myDate.getDay())
    }
    dateArr.push(arrItem)
  }
  return dateArr
}
