import REQUEST_KEY from '../config/request'
const BaseUtil = {
  /**
   * 设置表单嵌套对象数据
   * @author: Enzo
   * @date: 2019-10-31
   */
  setFormData (formData, resData = {}) {
    Object.keys(formData).forEach(item => {
      if (typeof formData[item] === 'object') {
        if (formData[item] instanceof Array) {
          // 数组暂不处理
        } else {
          Object.keys(formData[item]).forEach(item1 => {
            // eslint-disable-next-line no-prototype-builtins
            if (resData[item].hasOwnProperty(item1)) {
              formData[item][item1] = resData[item][item1]
            }
          })
        }
      } else {
        formData[item] = resData[item]
      }
    })
    return formData
  },

  /**
   * 获得文件大小
   * @author: Enzo
   * @date: 2019-11-6
   */
  getFileSize (size) {
    if (size < 1024) {
      return size + 'B'
    } else if (size / 1024 < 1024) {
      return Math.round(size / 1024 * 100) / 100.0 + 'KB'
    } else if (size / 1024 / 1024 < 1024) {
      return Math.round(size / 1024 / 1024 * 100) / 100.0 + 'MB'
    } else if (size / 1024 / 1024 / 1024 < 1024 * 100) {
      return Math.round(size / 1024 / 1024 / 1024 * 100) / 100.0 + 'GB'
    }
  },
  /**
   * 下载文件
   * @author Enzo
   * @date 2019-11-27
   */
  downFile (downPath, fileName) {
    const link = document.createElement('a')
    link.style.display = 'none'
    link.href = downPath
    if (fileName != null) {
      link.setAttribute('download', fileName)
    }
    document.body.appendChild(link)
    link.click()
  },
  /**
   * 下载文件
   * @author: Enzo
   * @date: 2019-11-6
   */
  downUploadFile (fileLog) {
    const downPath = BaseUtil.getUploadPath(fileLog.uploadPath)
    const fileName = fileLog.oldFileName
    BaseUtil.downFile(downPath, fileName)
  },
  downUploadFilePath (path, fileName) {
    const downPath = BaseUtil.getUploadPath(path)
    BaseUtil.downFile(downPath, fileName)
  },
  /**
   * 下载临时文件
   * @author: Enzo
   * @date: 2019-11-9
   */
  downTempFile (path, fileName) {
    const downPath = BaseUtil.getTempPath(path)
    BaseUtil.downFile(downPath, fileName)
  },
  /**
   * 下载备份文件
   * @author Enzo
   * @date 2019-11-27
   */
  downBackupFile (path, fileName) {
    const downPath = BaseUtil.getBackupPath(path)
    BaseUtil.downFile(downPath, fileName)
  },
  /**
   * 打开、预览文件
   * @author: Enzo
   * @date: 2019-11-6
   */
  openUploadFile (fileLog) {
    const downPath = BaseUtil.getUploadPath(fileLog.uploadPath)
    window.open(downPath)
  },
  /**
   * 获得上传文件访问路径
   * @author: Enzo
   * @date: 2019-11-20
   */
  getUploadFilePath (fileLog) {
    const downPath = BaseUtil.getUploadPath(fileLog.uploadPath)
    return downPath
  },
  /**
   * 获得备份文件路径
   * @author Enzo
   * @date 2019-11-27
   */
  getBackupPath (path) {
    if (path.indexOf('http') === 0) {
      return path
    }
    // return "api/backup/" + path;
    return REQUEST_KEY.requestParams.prefix + REQUEST_KEY.fileConfig.backupPrefix + path.replace(/\\/g, '/')
  },
  /**
   * 获得上传文件路径
   * @author Enzo
   * @date 2019-11-27
   */
  getUploadPath (path) {
    console.log(path)
    if (path.indexOf('http') === 0) {
      return path
    }
    // return baseUrl + '/upload/' + path.replace(/\\/g,'/');
    // return "http://114.116.221.4:9001/upload/" + path.replace(/\\/g,'/');
    return `${REQUEST_KEY.requestParams.prefix}${REQUEST_KEY.fileConfig.uploadPrefix}${path.replace(/\\/g, '/')}`
  },
  /**
   * 获得临时文件路径
   * @author Enzo
   * @date 2019-11-27
   */
  getTempPath (path) {
    if (path.indexOf('http') === 0) {
      return path
    }
    return REQUEST_KEY.requestParams.prefix + REQUEST_KEY.fileConfig.tempPrefix + path.replace(/\\/g, '/')
    // return "temp/" + path;
  },
  guid () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0
      var v = c === 'x' ? r : (r & 0x3 | 0x8)
      return v.toString(16)
    })
  }
}
export default BaseUtil
