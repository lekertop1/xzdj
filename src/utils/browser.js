export const userAgent = window.navigator.userAgent.toLowerCase()
export const version = window.navigator.appVersion
export const language = (window.navigator.language || window.navigator.browserLanguage).toLowerCase()

/**
 * 获取浏览器标识
 * @returns {string}
 */
export function getUserAgent () {
  return userAgent
}

/**
 * 获取浏览器版本
 * @returns {string}
 */
export function getVersion () {
  return version
}

/**
 * 获取浏览器语言信息
 * @returns {string}
 */
export function getLanguage () {
  return language
}

/**
 * 判断是否ie内核
 * @returns {boolean}
 */
export function isTrident () {
  return !!userAgent.match(/Trident/i)
}

/**
 * 是否为opera内核
 * @returns {boolean}
 */
export function isPresto () {
  return !!userAgent.match(/Presto/i)
}
/**
 * 是否为苹果、谷歌内核
 * @returns {boolean}
 */
export function isWebKit () {
  return !!userAgent.match(/AppleWebKit/i)
}
/**
 * 是否为火狐内核
 */
export function isGecko () {
  return !!userAgent.match(/Gecko/i) && userAgent.indexOf('KHTML') === -1
}
/**
 * 是否为移动终端
 * @returns {boolean}
 */
export function isMobile () {
  return !!userAgent.match(/AppleWebKit.*Mobile.*/i)
}
/**
 * ios终端
 * @returns {boolean}
 */
export function isIOS () {
  return !!userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/i)
}
/**
 * android终端
 * @returns {boolean}
 */
export function isAndroid () {
  return !!userAgent.match(/Android/i) || !!userAgent.match(/Adr/i)
}
/**
 * 是否为iPhone或者QQHD浏览器
 * @returns {boolean}
 */
export function isIPhone () {
  return !!userAgent.match(/iPhone/i)
}
/**
 * 是否iPad
 * @returns {boolean}
 */
export function isIPad () {
  return !!userAgent.match(/iPad/i)
}
/**
 * 是否web应该程序，没有头部与底部
 * @returns {boolean}
 */
export function isWebApp () {
  return !!userAgent.indexOf(/Safari/i)
}
/**
 * 是否是微信客户端
 * @returns {boolean}
 */
export function isWeChat () {
  return userAgent.match(/MicroMessenger/i).indexOf('micromessenger') > -1
}
/**
 * 是否QQ客户端
 * @returns {boolean}
 */
export function isQQ () {
  return userAgent.match(/\sQQ/i) === 'qq'
}
/**
 * 是否为mac终端
 * @returns {boolean}
 */
export function isMac () {
  return /macintosh|mac os x/i.test(userAgent)
}
/**
 * 是否为windows终端
 * @returns {boolean}
 */
export function isWindows () {
  return /windows|win32/i.test(userAgent)
}

/**
 * 是否为safari浏览器
 * @returns {boolean}
 */
export function isSafari () {
  return /Safari/i.test(userAgent) && !/Chrome/i.test(userAgent)
}
