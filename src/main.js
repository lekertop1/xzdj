'use strict'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ViewUI from 'view-design'
// 主要以iview组件库为主，element按需引入（切记）
import { Carousel, CarouselItem, Scrollbar, Tree } from 'element-ui'
import 'view-design/dist/styles/iview.css'
import './assets/icon-font/iconfont.css'
import rem from './utils/rem.js'
// 导入全局指令
import directive from './directive/index'
// 导入全局过滤器
import filters from './filters/index'
import SlideVerify from 'vue-monoplasty-slide-verify'
Vue.use(rem).use(ViewUI).use(directive).use(filters).use(SlideVerify).use(Carousel).use(CarouselItem).use(Scrollbar).use(Tree)
Vue.config.productionTip = false
window.echarts = require('echarts')
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
