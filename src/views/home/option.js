const echarts = require('echarts')
export default (datalist) => ({
  educationOption: {
    tooltip: {
      trigger: 'item'
    },
    series: [{
      name: '学历情况',
      type: 'pie',
      radius: '40%',
      data: datalist,
      emphasis: {
        itemStyle: {
          shadowBlur: 10,
          shadowOffsetX: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)'
        }
      },
      itemStyle: {
        normal: {
          label: {
            show: true,
            position: 'outside',
            color: '#fff',
            formatter: function (params) {
              var percent = 0
              var total = 0
              for (var i = 0; i < datalist.length; i++) {
                total += datalist[i].value
              }
              percent = ((params.value / total) * 100).toFixed(0)
              return params.name + percent + '%'
            }
          }
        }
      }
    }]
  },
  evaluation: {
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data: ['总分', '自评分', '区平均分'],
      textStyle: {
        color: '#fff',
        fontSize: 13
      }
    },
    xAxis: {
      type: 'category',
      data: ['阵地建设', '支部运行', '队伍管理', '统领作用'],
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#FFFFFF'
        }
      },
      axisLabel: {
        margin: 10,
        color: '#fff',
        textStyle: {
          fontSize: 19
        }
      }
    },
    yAxis: {
      type: 'value',
      min: 5,
      max: 40,
      splitNumber: 8,
      axisLabel: {
        color: '#fff',
        textStyle: {
          fontSize: 16
        }
      }
    },
    grid: {
      bottom: '8%'
    },
    series: [
      {
        name: '总分',
        type: 'line',
        data: [20, 25, 20, 35],
        itemStyle: {
          normal: {
            color: '#00E0FF'
          }
        },
        label: {
          normal: {
            show: true,
            lineHeight: 30,
            formatter: '{c}',
            position: 'top',
            textStyle: {
              color: '#00E0FF',
              fontSize: 16
            }
          }
        }
      },
      {
        name: '自评分',
        type: 'line',
        data: [18, 24, 16, 29],
        itemStyle: {
          normal: {
            color: '#018AF4'
          }
        },
        label: {
          normal: {
            show: true,
            lineHeight: 30,
            formatter: '{c}',
            position: 'top',
            textStyle: {
              color: '#018AF4',
              fontSize: 16
            }
          }
        }
      },
      {
        name: '区平均分',
        type: 'line',
        data: [16, 21, 10, 31],
        itemStyle: {
          normal: {
            color: '#F60E95'
          }
        },
        label: {
          normal: {
            show: true,
            lineHeight: 30,
            formatter: '{c}',
            position: 'top',
            textStyle: {
              color: '#F60E95',
              fontSize: 16
            }
          }
        }
      }
    ]
  },
  collectiveEconomy: {
    title: {
      text: '单位：万元',
      left: '80%',
      top: '5%',
      textStyle: {
        color: '#fff',
        fontSize: 17,
        align: 'center'
      }
    },
    xAxis: {
      type: 'category',
      data: ['2018', '2019', '2020'],
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#FFFFFF'
        },
        symbol: ['none', 'arrow']
      },
      axisLabel: {
        margin: 10,
        color: '#fff',
        textStyle: {
          fontSize: 19
        }
      }
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        color: '#fff',
        textStyle: {
          fontSize: 16
        }
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: '#FFFFFF'
        },
        symbol: ['none', 'arrow']
      },
      splitLine: {
        lineStyle: {
          type: 'dashed' // 设置网格线类型 dotted：虚线   solid:实线
        }
      }
    },
    grid: {
      bottom: '8%'
    },
    legend: {
      data: ['经营性收入', '经常性收入', '全区平均'],
      textStyle: {
        color: '#fff',
        fontSize: 13
      }
    },
    series: [{
      name: '经营性收入',
      data: [50, 72, 103],
      type: 'bar',
      itemStyle: {
        normal: {
          borderWidth: 1,
          barBorderRadius: 18,
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(25, 168, 168, 1)' // 0% 处的颜色
          }, {
            offset: 1,
            color: 'rgba(18, 116, 120, 1)' // 100% 处的颜色
          }], false)
        }
      }
    }, {
      name: '经营性收入',
      type: 'pictorialBar',
      z: 3,
      itemStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(25, 168, 168, 1)' // 0% 处的颜色
        }, {
          offset: 1,
          color: 'rgba(18, 116, 120, 1)' // 100% 处的颜色
        }], false)
      },
      symbol: 'circle',
      symbolSize: ['25', '15'],
      symbolPosition: 'end',
      symbolOffset: [-46, 0],
      data: [50, 72, 103]
    }, {
      name: '全区平均',
      data: [53, 70, 99],
      type: 'bar',
      itemStyle: {
        normal: {
          borderWidth: 1,
          // borderColor: '#18CEE2',
          barBorderRadius: 18,
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(208, 74, 86, 1)' // 0% 处的颜色
          }, {
            offset: 1,
            color: 'rgba(186, 43, 75, 1)' // 100% 处的颜色
          }], false)
        }
      }
    }, {
      name: '全区平均',
      type: 'pictorialBar',
      z: 3,
      itemStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(208, 74, 86, 1)' // 0% 处的颜色
        }, {
          offset: 1,
          color: 'rgba(186, 43, 75, 1)' // 100% 处的颜色
        }], false)
      },
      symbol: 'circle',
      symbolSize: ['25', '15'],
      symbolPosition: 'end',
      symbolOffset: [-15, 0],
      data: [53, 70, 99]
    }, {
      name: '经常性收入',
      data: [110, 190, 245],
      type: 'bar',
      itemStyle: {
        normal: {
          borderWidth: 1,
          // borderColor: '#18CEE2',
          barBorderRadius: 18,
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(220, 214, 42, 1)' // 0% 处的颜色
          }, {
            offset: 1,
            color: 'rgba(182, 138, 46, 1)' // 100% 处的颜色
          }], false)
        }
      }
    }, {
      name: '经常性收入',
      type: 'pictorialBar',
      z: 3,
      itemStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(220, 214, 42, 1)' // 0% 处的颜色
        }, {
          offset: 1,
          color: 'rgba(182, 138, 46, 1)' // 100% 处的颜色
        }], false)
      },
      symbol: 'circle',
      symbolSize: ['25', '15'],
      symbolPosition: 'end',
      symbolOffset: [15, 0],
      data: [110, 190, 245]
    }, {
      name: '全区平均',
      data: [120, 182, 190],
      type: 'bar',
      itemStyle: {
        normal: {
          borderWidth: 1,
          // borderColor: '#18CEE2',
          barBorderRadius: 18,
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(208, 74, 86, 1)' // 0% 处的颜色
          }, {
            offset: 1,
            color: 'rgba(186, 43, 75, 1)' // 100% 处的颜色
          }], false)
        }
      }
    }, {
      name: '全区平均',
      type: 'pictorialBar',
      z: 3,
      itemStyle: {
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
          offset: 0,
          color: 'rgba(208, 74, 86, 1)' // 0% 处的颜色
        }, {
          offset: 1,
          color: 'rgba(186, 43, 75, 1)' // 100% 处的颜色
        }], false)
      },
      symbol: 'circle',
      symbolSize: ['25', '15'],
      symbolPosition: 'end',
      symbolOffset: [47, 0],
      data: [120, 182, 190]
    }]
  },
  universityOption: {
    title: {
      text: '单位：人',
      left: '5%',
      top: '5%',
      textStyle: {
        color: 'rgba(0, 224, 255, 1)',
        fontSize: 13,
        align: 'center'
      }
    },
    xAxis: {
      type: 'category',
      data: ['2018', '2019', '2020'],
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#FFFFFF'
        },
        symbol: ['none', 'arrow']
      },
      axisLabel: {
        margin: 10,
        color: '#fff',
        textStyle: {
          fontSize: 19
        }
      }
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        color: '#fff',
        textStyle: {
          fontSize: 16
        }
      },
      axisLine: {
        show: true,
        lineStyle: {
          color: '#FFFFFF'
        },
        symbol: ['none', 'arrow']
      },
      axisTick: {
        show: true
      },
      splitLine: {
        show: false
      }
    },
    grid: {
      left: '45px',
      bottom: '18%'
    },
    series: [{
      name: 'hill',
      type: 'pictorialBar',
      symbol: 'triangle',
      barWidth: 75,
      itemStyle: {
        normal: {
          color: function (params) {
            var colorList = [['RGBA(221, 32, 52, 1)', 'RGBA(221, 32, 52, 1)'], ['RGBA(247, 234, 21, 1)', 'RGBA(239, 151, 73, 1)'], ['RGBA(0, 105, 53, 1)', 'RGBA(114, 188, 128, 1)']]
            var color = new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
              offset: 0,
              color: colorList[params.dataIndex][0] // 0% 处的颜色
            }, {
              offset: 1,
              color: colorList[params.dataIndex][1] // 100% 处的颜色
            }], false)
            return color
          }
        },
        emphasis: {
          opacity: 1
        }
      },
      data: [8, 10, 15],
      z: 10
    }, {
      name: 'glyph',
      type: 'pictorialBar',
      barGap: '-20%',
      symbolPosition: 'end',
      symbolSize: 50,
      symbolOffset: [0, '-120%']
    }]
  },
  personnelOption: {
    series: [{
      type: 'gauge',
      startAngle: 180,
      endAngle: 0,
      min: 0,
      max: 240,
      splitNumber: 12,
      radius: '150%',
      center: ['50%', '80%'],
      itemStyle: {
        color: 'RGBA(217, 31, 57, 1)'
      },
      progress: {
        show: true,
        roundCap: false,
        width: 25
      },
      pointer: {
        show: false
      },
      axisLine: {
        roundCap: false,
        lineStyle: {
          width: 25
        }
      },
      axisTick: {
        show: false
      },
      splitLine: {
        show: false
      },
      axisLabel: {
        show: false
      },
      title: {
        show: false
      },
      detail: {
        show: true,
        offsetCenter: [0, '-5%'],
        formatter: function (value) {
          return '{value|' + value.toFixed(0) + '%' + '}'
        },
        rich: {
          value: {
            color: '#fff',
            fontSize: '31px'
          }
        }
      },
      data: [{
        value: 96
      }]
    }]
  }
})
