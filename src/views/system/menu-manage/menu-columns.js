/*
 * @message: 菜单表格配置
 */
const columns = [{
  title: '菜单名称',
  key: 'menuName',
  minWidth: 150
}, {
  title: '菜单编号',
  key: 'menuCode',
  minWidth: 150
}, {
  title: '菜单url',
  minWidth: 150,
  key: 'linkUrl'
}, {
  title: '菜单完整名称',
  key: 'displayName',
  tooltip: true,
  minWidth: 240
}, {
  title: '排序',
  minWidth: 70,
  key: 'sortCode'
}, {
  title: '菜单图标',
  key: 'icon',
  minWidth: 160
}, {
  title: '站点',
  key: 'siteCode',
  minWidth: 90
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
export default columns
