/*
 * @message: 消息模块表格配置
 */
const columns = [{
  title: '消息模板名称',
  key: 'templateName',
  minWidth: 150
}, {
  title: '模板key',
  key: 'templateCode',
  minWidth: 270
}, {
  title: '消息模板内容',
  key: 'templateContent',
  tooltip: true,
  minWidth: 270
}, {
  title: '消息状态',
  key: 'sendSwitch',
  width: '120px',
  render: (h, params) => {
    const map = ['不发送', '发送']
    return h('Tag', {
      attrs: {
        color: params.row.sendSwitch === 1 ? 'success' : 'error'
      }
    }, map[params.row.sendSwitch])
  }
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
export default columns
