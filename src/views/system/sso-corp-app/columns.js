/*
 * @message: 集成应用管理表格配置
 */
import { requestGet } from '../../../utils/request'
import API_KEY from '../../../api'

const columns = [{
  title: '企业名称',
  key: 'corpName',
  minWidth: 120
}, {
  title: '应用名称',
  key: 'appName',
  tooltip: true,
  minWidth: 180
}, {
  title: '回调域名',
  key: 'redirectUri',
  render: (h, params) => {
    return h('span', {
      style: {
        color: '#1051e3',
        cursor: 'pointer'
      },
      on: {
        click: () => {
          const param = { appId: params.row.appId, state: '2' }
          requestGet(API_KEY.dcSso.getSsoTempAuthCode, param).then(({ data }) => {
            window.open(data)
          })
        }
      }
    },
    params.row.redirectUri)
  },
  minWidth: 250
}, {
  title: 'appId',
  key: 'appId',
  minWidth: 290
}, {
  title: 'appSecret',
  key: 'appSecret',
  minWidth: 290
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
export default columns
