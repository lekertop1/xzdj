/*
 * @message: 系统参数配置
 */
const columns = [{
  title: '参数名称',
  key: 'parameterName',
  minWidth: 170
}, {
  title: '参数类型编号',
  key: 'parameterGroup',
  tooltip: true,
  minWidth: 110
}, {
  title: '参数编号',
  key: 'parameterCode',
  minWidth: 100
}, {
  title: '参数值',
  key: 'parameterValue',
  minWidth: 150,
  tooltip: true
}, {
  title: '配置环境',
  key: 'effectiveEnvironment',
  width: 100
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
export default columns
