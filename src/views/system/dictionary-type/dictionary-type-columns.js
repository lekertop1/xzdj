/*
 * @message: 数据字典类别表格配置
 */
const columns = [{
  title: '数据字典类别名称',
  key: 'itemName'
}, {
  title: '数据字典类别编号',
  key: 'itemCode'
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
export default columns
