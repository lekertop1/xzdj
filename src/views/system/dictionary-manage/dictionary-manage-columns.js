/*
* @message: 数据字典表格配置
 */
const columns = [{
  title: '数据字典名称',
  key: 'itemName',
  minWidth: 120
}, {
  title: '字典编号',
  key: 'itemCode',
  minWidth: 100
}, {
  title: '参数值',
  key: 'itemValue',
  minWidth: 120
}, {
  title: '排序',
  key: 'sortCode',
  minWidth: 80
}, {
  title: '状态',
  key: 'state',
  render: (h, params) => {
    const arr = ['不可修改', '可修改', '禁用']
    return h('Tag', {
      attrs: {
        color: params.row.state !== 1 ? 'error' : 'success'
      }
    }, arr[params.row.state])
  },
  minWidth: 100
}, {
  title: '备注',
  key: 'memo',
  tooltip: true,
  minWidth: 100
},
{
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
export default columns
