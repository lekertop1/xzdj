const columns = [{
  title: '序号',
  type: 'index',
  align: 'center',
  width: 80
}, {
  header: '农村名称',
  key: 'rfRuralName',
  align: 'center',
  tooltip: false,
  minWidth: 150
}, {
  title: '网格名称',
  key: 'gridName',
  align: 'center',
  minWidth: 150
}, {
  title: '网格评级',
  key: 'gridLevel',
  align: 'center',
  minWidth: 150
},
{
  title: '党员数',
  key: 'partyMemberNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '户籍人数',
  key: 'registeredPopulationNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '户籍人数',
  key: 'registeredPopulationNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '网格坐标',
  key: 'coordinates',
  minWidth: 150
},
{
  title: '操作',
  slot: 'action',
  minWidth: 230
}]
export default columns
