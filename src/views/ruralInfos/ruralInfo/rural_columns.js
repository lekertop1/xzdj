const renderFun = function (name) {
  return (h, params) => {
    return h('img', {
      attrs: {
        src: params.row[name], style: 'width: 100px;border-radius: 2px;'
      }
    })
  }
}
const columns = [{
  title: '序号',
  type: 'index',
  align: 'center',
  width: 80
}, {
  title: '农村名称',
  key: 'rfRuralName',
  align: 'center',
  tooltip: false,
  minWidth: 150
}, {
  title: '服务中心图片',
  render: renderFun('centerPhoto'),
  align: 'center',
  minWidth: 150
}, {
  title: '集体照',
  key: 'groupPhoto',
  align: 'center',
  minWidth: 150
}, {
  title: '党组织书记姓名',
  key: 'secretaryName',
  align: 'center',
  minWidth: 150
},
{
  title: '党组织书记照片',
  key: 'secretaryPhoto',
  align: 'center',
  minWidth: 150
},
{
  title: '党组织书记荣誉',
  key: 'secretaryHonor',
  align: 'center',
  minWidth: 150
},
{
  title: '党组织书记标签',
  key: 'secretaryTag',
  align: 'center',
  minWidth: 150
},
{
  title: '两委男班子数',
  key: 'committee_man_number',
  align: 'center',
  minWidth: 150
},
{
  title: '两委女班子数',
  key: 'committeeWomanNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '总人口数',
  key: 'populationNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '户数',
  key: 'householdsNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '常住人口数',
  key: 'residentsPopulationNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '户籍人口数',
  key: 'registeredPopulationNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '流动人口数',
  key: 'floatingPopulationNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '党员人数',
  key: 'partyMemberNumber',
  align: 'center',
  minWidth: 150
},
{
  title: '操作',
  slot: 'action',
  minWidth: 230
}
]
export default columns
