const columns = [{
  title: '序号',
  type: 'index',
  align: 'center',
  width: 80
}, {
  header: '农村名称',
  key: 'rfRuralName',
  align: 'center',
  tooltip: false,
  minWidth: 150
}, {
  title: '网格数',
  key: 'gridNumber',
  align: 'center',
  minWidth: 150
}, {
  title: '微网格数',
  key: 'microGridNumber',
  align: 'center',
  minWidth: 150
},
{
  title: '操作',
  slot: 'action',
  minWidth: 230
}]
export default columns
