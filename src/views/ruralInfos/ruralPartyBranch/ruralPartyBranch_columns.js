const columns = [{
  title: '序号',
  type: 'index',
  align: 'center',
  width: 80
}, {
  header: '农村名称',
  key: 'rfRuralName',
  align: 'center',
  tooltip: false,
  minWidth: 150
}, {
  title: '统计年份',
  key: 'statisticsYear',
  align: 'center',
  minWidth: 150
}, {
  title: '党员活动平均参与率',
  key: 'activityRate',
  align: 'center',
  minWidth: 150
}, {
  title: '操作',
  slot: 'action',
  minWidth: 230
}]
export default columns
