const columns = [{
  title: '用户账号',
  minWidth: 120,
  key: 'userAccount',
  ellipsis: true,
  width: '150px'
}, {
  title: '用户姓名',
  key: 'userName',
  tooltip: true,
  minWidth: 100,
  width: '100px'
}, {
  // title: '手机号码',
  // minWidth: 120,
  // render: (h, params) => {
  //   console.log(params)
  //   return h('span', params.row.userInfo.phone)
  // }
}, {
  title: '所属单位',
  key: 'orgNames',
  tooltip: true,
  minWidth: 300,
  width: '200px'
}, {
  title: '所属部门',
  key: 'deptNames',
  tooltip: true,
  minWidth: 150,
  width: '200px'
}, {
  title: '所属角色',
  key: 'roleNames',
  ellipsis: true,
  minWidth: 300,
  width: '200px'
}, {
  title: '激活状态',
  slot: 'drop',
  key: 'activeStatus',
  minWidth: 140,
  width: '100px'
}, {
  title: '创建人',
  key: 'createUserName',
  minWidth: 120,
  width: '100px'
}, {
  title: '注册时间',
  key: 'gmtCreate',
  tooltip: true,
  minWidth: 170,
  width: '170px'
}, {
  title: '最后登录时间',
  tooltip: true,
  key: 'lastLoginTime',
  minWidth: 170,
  width: '170px'
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '200px',
  slot: 'action'
}]
export default columns
