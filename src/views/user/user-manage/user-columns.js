const tableItem = [{
  label: '账号:',
  minWidth: 100,
  key: 'userAccount'
}, {
  label: '用户姓名:',
  // customEl: { type: 'tag', color: 'blue'},
  key: 'userName',
  minWidth: 70,
  style: { 'margin-right': '10px' }
}, {
  label: '部门:',
  key: 'deptNames',
  style: { width: '230px' },
  tooltip: true
}, {
  label: '单位:',
  // style: { color: 'rgba(31,93,234,1)' },
  key: 'orgNames',
  style: { width: '300px' }
}, {
  customEl: { type: 'tag', color: 'blue' },
  key: 'roleNames',
  minWidth: 110
}, {
  label: '激活状态:',
  customEl: { type: 'select', size: 'small', elText: [{ label: '禁用', value: 0 }, { label: '激活', value: 1 }, { label: '待审批', value: 2 }, { label: '锁定', value: 3 }] },
  key: 'activeStatus',
  minWidth: 69
}, {
  key: 'createUserName',
  minWidth: 100
}, {
  key: 'createTime',
  style: { width: '130px' }
}]

const columns = [{
  type: 'selection',
  width: '35px'
},
{
  slot: 'special'
}]
export { tableItem, columns }
