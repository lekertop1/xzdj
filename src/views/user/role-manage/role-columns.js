/*
 * @message: 角色管理表格配置
 */
const columns = [{
  title: '角色名称',
  key: 'roleName',
  minWidth: 120
}, {
  title: '所属单位',
  key: 'orgName',
  minWidth: 210
}, {
  title: '更新人',
  key: 'updateUserName',
  minWidth: 100
}, {
  title: '更新时间',
  key: 'gmtModified',
  minWidth: 170
}, {
  title: '备注',
  key: 'memo',
  minWidth: 170
},
{
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
export default columns
