/*
 * @message: 部门表格配置
 */
const columns = [{
  title: '部门名称',
  key: 'deptName',
  minWidth: 100
}, {
  title: '部门地址',
  key: 'address',
  minWidth: 160,
  tooltip: true
}, {
  title: '负责人',
  key: 'contacts',
  minWidth: 90
}, {
  title: '所属单位',
  key: 'orgName',
  minWidth: 210
}, {
  title: '更新人',
  key: 'updateUserName',
  minWidth: 95
}, {
  title: '更新时间',
  key: 'gmtModified',
  minWidth: 170
},
{
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
export default columns
