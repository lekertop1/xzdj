/*
 * @message: 单位管理表格配置
 */
const columns = [{
  title: '单位名称',
  key: 'orgName',
  minWidth: 210,
  tooltip: true
}, {
  title: '联系人',
  key: 'contacts',
  minWidth: 100
}, {
  title: '联系电话',
  key: 'phone',
  minWidth: 130
}, {
  title: '单位地址',
  key: 'address',
  minWidth: 110
}, {
  title: '更新人',
  key: 'updateUserName',
  minWidth: 90
}, {
  title: '更新时间',
  key: 'gmtModified',
  minWidth: 170
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '160px',
  slot: 'action'
}]
// 默认导出
export default columns
