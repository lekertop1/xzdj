/*
 * @message: 头部注释
 */
const columns = [{
  title: '登录时间',
  minWidth: 50,
  key: 'loginDate',
  width: '180px'
}, {
  title: '账号',
  tooltip: true,
  key: 'userAccount',
  width: '120px'
}, {
  title: '登陆类型',
  key: 'loginType',
  width: '120px',
  render: (h, params) => {
    const arr = ['WEB', '安卓', '苹果', '钉钉', '微信小程序',
      '微信公众号', '微信扫码登录', 'qq扫码登录', '钉钉扫码登录',
      '邮箱验证码登录', '手机验证码登录', 'App', 'H5']
    return h('span', arr[params.row.loginType])
  }
}, {
  title: '登出类型',
  key: 'loginOutType',
  width: '150px',
  render: (h, params) => {
    const arr = ['', '注销', '重复登录掉线', '登录超时', '切换账号']
    return h('span', arr[params.row.loginOutType])
  }
}, {
  title: '登录IP',
  minWidth: 60,
  key: 'requestIp',
  width: '180px'
}, {
  title: '登录状态',
  render: (h, params) => {
    const loginState = params.row.loginState
    let str = ''
    switch (loginState) {
      case 1:
        str = '成功'
        break
      case 2:
        str = '帐号不存在'
        break
      case 3:
        str = '密码错误'
        break
      case 4:
        str = '帐号未激活'
        break
      case 5:
        str = '帐号被锁定'
        break
      default:
        str = '失败'
        break
    }
    return h('span', str)
  },
  width: '120px'
}, {
  title: '登录类型',
  render: (h, params) => {
    const loginType = params.row.loginType
    let str = ''
    switch (loginType) {
      case 0:
        str = 'PC'
        break
      case 2:
        str = '钉钉'
        break
      case 3:
        str = '单点登录'
        break
      default:
        str = '未知'
    }
    return h('span', str)
  },
  width: '120px'
}, {
  title: '手机型号/浏览器型号',
  tooltip: true,
  minWidth: 50,
  key: 'modelNumber'
}]
export default columns
