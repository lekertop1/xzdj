/*
 * @message: 备份表格配置
 */
const columns = [{
  title: '备份文件名称',
  key: 'backupName',
  minWidth: 210,
  tooltip: true
}, {
  title: '文件大小',
  minWidth: 100,
  key: 'fileSizeStr'
}, {
  title: '备份类型',
  minWidth: 120,
  key: 'backupType',
  render: (h, params) => {
    const arr = ['完整备份', '增量备份', '数据结构备份', '导入备份']
    return h('span', arr[params.row.backupType])
  }
}, {
  title: '备份人',
  key: 'createUserName',
  minWidth: 90
}, {
  title: '备份时间',
  key: 'gmtCreate',
  minWidth: 170
}, {
  title: '还原执行次数',
  key: 'executeCount',
  minWidth: 120
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '150px',
  slot: 'action'
}]
export default columns
