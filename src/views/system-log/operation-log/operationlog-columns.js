/*
 * @message: 操作表格配置
 */
const columns = [{
  title: '账号',
  key: 'rfUserAccount',
  minWidth: 120
}, {
  title: '姓名',
  key: 'rfUserName',
  tooltip: true,
  minWidth: 100
}, {
  key: 'operationType',
  title: '操作类型',
  minWidth: 95
}, {
  key: 'menuName',
  title: '操作菜单',
  tooltip: true,
  minWidth: 180
}, {
  key: 'gmtCreate',
  title: '操作时间',
  minWidth: 180
}, {
  title: '操作ip',
  key: 'requestIp',
  minWidth: 150
}, {
  key: 'requestParams',
  title: '操作内容',
  minWidth: 150,
  tooltip: true
}]
export default columns
