/*
 * @message: 版本表格配置
 */
const columns = [{
  title: '版本号',
  key: 'versionCode',
  minWidth: 150
}, {
  title: '版本序列号',
  key: 'serializeNumber',
  minWidth: 120
}, {
  title: '发布时间',
  key: 'gmtCreate',
  minWidth: 180
}, {
  title: '更新内容',
  key: 'updateIntro',
  tooltip: true,
  minWidth: 200
}, {
  title: '操作',
  fixed: 'right',
  align: 'center',
  width: '150px',
  slot: 'action'
}]
export default columns
