/*
 * @message: 文件上传列表配置
 */
const columns = [{
  title: '文件名',
  key: 'oldFileName',
  tooltip: true,
  minWidth: 100
}, {
  title: '文件地址',
  key: 'uploadPath',
  minWidth: 480
}, {
  title: '文件类型',
  key: 'fileType',
  width: '95px'
}, {
  title: '文件大小',
  width: '95px',
  key: 'fileSizeStr'
  // render: (h, params) => {
  //   return h('span', BaseUtil.getFileSize(params.row.fileSize))
  // }
}, {
  title: '上传时间',
  key: 'gmtCreate',
  width: '170px'
}, {
  title: '上传人',
  key: 'createUserName',
  width: '100px'
}]
export default columns
