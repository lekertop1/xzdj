/*
 * @message: 登录日志表格配置
 */
const columns = [{
  title: '账号',
  key: 'userAccount',
  width: '120px'
}, {
  title: '姓名',
  key: 'rfUserName',
  tooltip: true,
  width: '100px'
}, {
  title: '登录时间',
  key: 'loginDate',
  width: '170px'
}, {
  title: '登陆类型',
  key: 'loginType',
  width: '100px',
  render: (h, params) => {
    const arr = ['WEB', '安卓', '苹果', '钉钉', '微信小程序',
      '微信公众号', '微信扫码登录', 'qq扫码登录', '钉钉扫码登录',
      '邮箱验证码登录', '手机验证码登录', 'App', 'H5']
    return h('span', arr[params.row.loginType])
  }
}, {
  title: '登出类型',
  key: 'loginOutType',
  width: '120px',
  render: (h, params) => {
    const arr = ['', '注销', '重复登录掉线', '登录超时', '切换账号']
    return h('span', arr[params.row.loginOutType])
  }
}, {
  title: '登录ip',
  key: 'requestIp',
  width: '180px'
}, {
  title: '登录地点',
  key: 'loginAddress',
  width: '100px'
}, {
  title: '客户端型号',
  key: 'modelNumber',
  tooltip: true
}, {
  title: '登录状态',
  width: 120,
  render: (h, params) => {
    const { row } = params
    const loginState = row.loginState || 0
    const loginMessage = row.loginMessage || '登录失败'
    return h('Tag', {
      attrs: {
        color: loginState === 1 ? 'success' : 'error'
      }
    }, loginMessage)
  }
}]
export default columns
