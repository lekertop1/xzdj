/*
 * @message: 消息发送记录表格配置
 */
const columns = [{
  title: '模板名称',
  key: 'rfTemplateName',
  minWidth: 120
}, {
  key: 'sender',
  title: '接收人',
  minWidth: 180
}, {
  key: 'msgContent',
  title: '消息内容',
  tooltip: true,
  minWidth: 300
}, {
  key: 'sendType',
  title: '发送类型',
  minWidth: 140,
  render: (h, params) => {
    const arr = ['无', '短信', '钉钉', '微信公众号', '微信小程序', '邮件', 'web推送', '安卓app推送', '苹果app推送']
    return h('span', arr[params.row.sendType])
  }
}, {
  title: '发送时间',
  key: 'gmtCreate',
  minWidth: 180
}, {
  title: '回执',
  key: 'receipt',
  minWidth: 200,
  tooltip: true
}]
export default columns
