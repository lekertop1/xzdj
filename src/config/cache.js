const enSysName = process.env.VUE_APP_SYS_EN_NAME
/**
 * cache参数信息
 * @type {{customVisit: string, customTabs: string, localStorageInfo: string, customSetting: string, tableHeader: string, localStoragePrefix: *}}
 */
export const CACHE_PARAM = {
  // 是否使用前缀
  localStoragePrefix: enSysName,
  // 缓存测试变量
  localStorageInfo: 'localStorageInfo',
  // 表头信息
  tableHeader: 'tableHeader',
  // 浏览信息
  customVisit: 'customVisit',
  // 选项卡信息
  customTabs: 'customTabs',
  // 设置信息
  customSetting: 'customSetting'
}
