// vue混入

import {
  createTableData,
  deleteTableByPk,
  getTableDataByoPk,
  updateTableData, updateTableDataWithEmptyValue
} from '../api/modules/table'
import message from '../utils/message'
import { typeOf } from '../utils/verification'
// 表格混入（目前需搭配BaseTable组件使用）
export const tableMixin = {
  data () {
    return {
      // 搜索关键字等
      params: {},
      // 视图参数（抽屉/对话框等）
      viewParams: {
        // 视图标题
        title: '',
        // 视图双向数据绑定
        flag: false
      },
      // 1为新增，2编辑，3查看
      type: -1,
      // 视图标题前缀和后缀
      suffix: '',
      prefix: '',
      filterData: [],
      // 是否刷新视图，状态发生变化就会刷新，而非为true,该值配合key使用
      isRefreshView: false
    }
  },
  // 导入BaseTable组件
  components: {
    BaseTable: () => import('@/components/view-ui/BaseTable'),
    Breadcrumb: () => import('@/components/view-ui/Breadcrumb')
  },
  methods: {
    /**
     * 获取表格数据
     */
    publicGetData (isSearch = false) {
      if (this.$refs.baseTable) {
        this.$refs.baseTable.getData(this.params, isSearch)
      } else {
        throw new Error('使用的BaseTable组件未定义相应ref，但却使用它在进行查询操作')
      }
    },
    /**
    * 列表高级查询
    * @author Enzo
    * @date 2020-6-23
    */
    publicAdvancedQueryData (params) {
      if (this.$refs.baseTable) {
        this.$refs.baseTable.getData(params)
      } else {
        throw new Error('使用的BaseTable组件未定义相应ref，但却使用它在进行查询操作')
      }
    },
    /**
     * 删除数据
     * @param params
     */
    publicRemoveData (params) {
      if (this.action && this.action.removeDataByKey) {
        // 调用删除接口
        deleteTableByPk(this.action.removeDataByKey, params.row[this.idName]).then((data) => {
          message.message.success(data.message)
          // 重新请求数据
          this.publicGetData()
        })
      } else {
        throw new Error('删除接口未定义或定义的删除接口格式错误')
      }
    },
    /**
     * 新增/编辑/查看
     * @param row
     * @param type 1/2/3分别对应新增/编辑/查看
     */
    publicUpdateData (row = {}, type = 1) {
      // 记录type
      this.type = type
      const arr = ['', '新增', '修改', '查看']
      if (typeOf(this[this.idName]) === 'undefined') {
        throw new Error('当前模块中未定义idName或模块id(比如：userId)但却使用它在进行相应操作')
      }
      this[this.idName] = (this.type === 2 || this.type === 3) ? row[this.idName] : -1
      // 设置视图标题
      this.viewParams.title = this.prefix + arr[this.type] + this.suffix
      // 打开视图
      this.viewParams.flag = true
    },
    /**
     * 关闭视图
     * @param code
     */
    publicCloseView (code = 0) {
      // 假如code为真，则关闭抽屉，并重新请求表格数据，否则就关闭抽屉
      this.viewParams.flag = false
      if (code) {
        // 重新请求数据
        this.publicGetData()
      }
    },
    /**
     *  视图状态发生变化时触发
     * @param {Boolean} flag
     */
    publicVisibleChange (flag) {
      if (flag) {
        this.isRefreshView = !this.isRefreshView
      }
    }
  }
}
// 新增/修改等混入
export const updateMixin = {
  props: {
    // 该参数必须传(需要该参数进行接口判断\请求判断等)
    type: {
      type: Number,
      required: true
    },
    // 模块id必须传
    moduleId: {
      type: [String, Number],
      required: true
    },
    // 模块id名称必须传
    idName: {
      type: String,
      required: true
    }
  },
  data () {
    return {
      // 定义请求地址(避免使用混入时没定义而报错，下同)
      action: {
        createData: '',
        updateData: ''
      },
      isInit: true,
      formValidate: {},
      formValidateView: {}
    }
  },
  // 初始化
  mounted () {
    if (this.isInit && this.type !== 1 && this.type !== -1) {
      this.$_init(this.moduleId)
    }
  },
  methods: {
    /**
     * 初始化
     */
    $_init (id) {
      if (this.type === 2) {
        // 新增id属性及version属性
        this.formValidate[this.idName] = id
        this.formValidate.version = null
      }
      // 根据id获取数据
      getTableDataByoPk(this.action.readDataById, id).then(({ data }) => {
        this.formValidateView = data || {}
        Object.keys(this.formValidate).forEach(item => {
          this.formValidate[item] = data[item]
        })
      })
    },
    /**
     * 数据提交
     * @param name
     */
    publicSubmit (name) {
      if (name) {
        // 判断校验是否通过
        this.$refs[name].validate((valid) => {
          if (valid) {
            // 如果用户提交前要对数据进行处理(data中定义isHandleSubmit属性，并且isHandleSubmit为true)
            if (typeOf(this.isHandleSubmit) === 'boolean' && this.isHandleSubmit && typeOf(this.beforeSubmit) === 'function') {
              // 定义beforeSubmit函数对数据进行处理，并返回一个promise对象
              this.beforeSubmit().then((params, demo) => {
                // 请求数据
                this.$_submitData(params)
              })
            } else {
              // 直接请求数据
              this.$_submitData(this.formValidate)
            }
          } else {
            message.message.error('输入格式错误')
          }
        })
      }
    },
    /**
     * 数据提交
     * @param name
     * @param ignore
     */
    publicSubmitWithEmptyString (name, ignore) {
      if (name) {
        // 判断校验是否通过
        this.$refs[name].validate((valid) => {
          if (valid) {
            // 如果用户提交前要对数据进行处理(data中定义isHandleSubmit属性，并且isHandleSubmit为true)
            if (typeOf(this.isHandleSubmit) === 'boolean' && this.isHandleSubmit && typeOf(this.beforeSubmit) === 'function') {
              // 定义beforeSubmit函数对数据进行处理，并返回一个promise对象
              this.beforeSubmit().then((params, demo) => {
                // 请求数据
                this.$_submitDataUpdateWithEmptyValue(params, ignore)
              })
            } else {
              // 直接请求数据
              this.$_submitData(this.formValidate)
            }
          } else {
            message.message.error('输入格式错误')
          }
        })
      }
    },
    /**
     * 数据提交
     * @param params
     */
    $_submitData (params) {
      if (this.type === 1) {
        // 发送请求
        createTableData(this.action.createData, params).then(data => {
          message.message.success(data.message)
          // 请求成功，关闭视图并重新请求数据
          this.$emit('closeView', 1)
        })
      } else if (this.type === 2) {
        // 发送请求
        updateTableData(this.action.updateData, params).then(data => {
          message.message.success(data.message)
          // 请求成功，关闭视图并重新请求数据
          this.$emit('closeView', 1)
        })
      }
    },
    /**
     * 数据提交
     * @param params
     * @param ignore
     */
    $_submitDataUpdateWithEmptyValue (params, ignore) {
      if (this.type === 1) {
        // 发送请求
        createTableData(this.action.createData, params).then(data => {
          message.message.success(data.message)
          // 请求成功，关闭视图并重新请求数据
          this.$emit('closeView', 1)
        })
      } else if (this.type === 2) {
        // 发送请求
        updateTableDataWithEmptyValue(this.action.updateData, params, ignore).then(data => {
          message.message.success(data.message)
          // 请求成功，关闭视图并重新请求数据
          this.$emit('closeView', 1)
        })
      }
    },
    /**
     * 取消数据提交
     * @param name
     */
    publicReset (name) {
      if (name && this.$refs[name]) {
        // 清空表单
        this.$refs[name].resetFields()
      }
      // 关闭视图
      this.$emit('closeView')
    }
  }
}

// 批量删除混入
export const deleteMixin = {
  data () {
    return {
      // 避免在使用混入时没定义而报错
      selection: []
    }
  },
  methods: {
    /**
     * 表格选项发生变化时触发
     * @param selection 选中的选项
     */
    publicSelectionChange (selection) {
      this.selection = selection
    },
    /**
     * 批量删除
     */
    publicDeleteBatch () {
      const selection = this.selection
      if (selection.length <= 0) {
        message.message.remove()
        message.message.error('删除失败')
      } else {
        message.modal.confirm('你确定要删除吗？', `此操作将删除<span style="color: red">${selection.length}</span>条数据，你确定还要继续吗？`, () => {
          if (!this.action.deletes) {
            throw new Error('删除失败')
          } else {
            // eslint-disable-next-line no-unused-vars
            let ids = ''
            selection.forEach(item => {
              ids += item[this.idName] + ';'
            })
            // 截取字符串
            ids = ids.substring(0, ids.length - 1)
            // deleteDataByIds(this.action.deletes, ids).then(data => {
            //   message.message.success(data.message)
            //   // 重新请求数据
            //   this.publicGetData()
            // })
          }
        }, () => {
          message.message.success('取消成功')
        })
      }
    }
  }
}
