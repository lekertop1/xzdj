import { requestGet } from '../../../utils/request'
import API_KEY from '../../index'

/**
 * 获取数据字典参数
 * @returns {*}
 */
export function getDictionaryGroupSelect () {
  return requestGet(API_KEY.dictionary.readDictionaryType, {})
}

export function getDictionarySelectName (dictionaryId, fkDictionaryGroupId) {
  const params = {}
  if (dictionaryId !== '') {
    params.dictionaryId = dictionaryId
  }
  if (fkDictionaryGroupId !== '') {
    params.fkDictionaryGroupId = fkDictionaryGroupId
  }
  return requestGet(API_KEY.dictionary.readDictionarySelect, params)
}

/**
 * 根据key获取数据字典下拉
 * @param key
 */
export function getDictionarySelectByKey (key) {
  return requestGet(`${API_KEY.dictionary.readDictionarySelectByKey}/${key}`, {})
}

/**
 * 获取数据字典下拉，（支持批量）
 * @param keys
 * @returns {*}
 */
export function getDictionarySelectByKeys (keys) {
  return requestGet(`${API_KEY.dictionary.readDictionarySelectByKeys}`, { groupKeys: keys })
}
/**
 * 根据节点编号获取数据字典信息
 * @param rootCode
 */
export function getDictionaryInfoByRootCode (rootCode) {
  return requestGet(API_KEY.dictionary.readDictionaryByRootCode + '' + rootCode, {})
}
