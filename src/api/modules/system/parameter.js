/*
 * @Author: duanyunlong
 * @since: 2020-07-23 17:12:54
 * @lastTime: 2020-07-23 17:50:53
 * @LastAuthor: Do not edit
 * @FilePath: \dc_container\src\api\modules\system\parameter.js
 * @message: 系统参数请求模块
 */
import { requestGet } from '../../../utils/request'
import API_KEY from '../../index'

/**
 * 获取谷歌系统参数
 */
export function getParameterById (params) {
  return requestGet(API_KEY.systemParameter.readParameter, params)
}
