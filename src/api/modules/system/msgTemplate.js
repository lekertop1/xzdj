/*
 * @Author: duanyunlong
 * @since: 2020-07-08 10:55:40
 * @lastTime: 2020-07-24 16:46:15
 * @LastAuthor: Do not edit
 * @FilePath: \dc_container\src\api\modules\system\msgTemplate.js
 * @message: 消息模块请求
 */
import { requestGet, requestPost, requestPut } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'

export function getById (id) {
  return requestGet(API_KEY.message.curdMessage + id, {})
}

export function updateMsg (params) {
  return requestPut(API_KEY.message.curdMessage, params, REQUEST_KEY.requestMode.json)
}

export function createMsg (params) {
  return requestPost(API_KEY.message.curdMessage, params, REQUEST_KEY.requestMode.json)
}
/**
* 发送邮箱验证码
* @author Enzo
* @date 2020-6-28
*/
export function sendMailVerificationCode (params) {
  return requestPost(API_KEY.sendMsg.sendMailVerificationCode, params, REQUEST_KEY.requestMode.json)
}
/**
* 发送短信验证码
* @author Enzo
* @date 2020-6-28
*/
export function sendPhoneVerificationCode (params) {
  return requestPost(API_KEY.sendMsg.sendPhoneVerificationCode, params, REQUEST_KEY.requestMode.json, REQUEST_KEY.responseType.json, { isError: true })
}
/**
 * 邮箱找回，重置密码
 */
export function retrievePasswordByMail (params) {
  return requestPut(API_KEY.sendMsg.retrievePasswordByMail, params, REQUEST_KEY.requestMode.json, REQUEST_KEY.responseType.json, { isError: true })
}
/**
 * 邮箱找回，重置密码
 */
export function retrievePasswordByPhone (params) {
  return requestPut(API_KEY.sendMsg.updatePasswordByPhone, params, REQUEST_KEY.requestMode.json, REQUEST_KEY.responseType.json, { isError: true })
}
