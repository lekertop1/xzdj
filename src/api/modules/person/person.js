import { requestGet, requestPut } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'

/**
 * 获取当前登录用户信息
 */
export function getPersonInformationUser () {
  return requestGet(API_KEY.user.getSysUser, {})
}
/**
 * 更新当前登录用户信息
 */
export function updateLoginUser (params) {
  return requestPut(API_KEY.user.updateLoginUser, params, REQUEST_KEY.requestMode.formData)
}
/**
 * 修改当前登录用户的登录密码
 */
export function updateMyPass (params) {
  return requestPut(API_KEY.user.updateMyPass, params, REQUEST_KEY.requestMode.formData)
}
