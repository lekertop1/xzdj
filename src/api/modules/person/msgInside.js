import { requestGet, requestPost } from '../../../utils/request'
import API_KEY from '../../index'

/**
 * 查询消息任务详情
 */
export function getById (id) {
  return requestGet(API_KEY.msgInside.def + id, {})
}
/**
 * 更新消息任务状态
 */
export function insert (params) {
  return requestPost(API_KEY.msgInside.def, params)
}
