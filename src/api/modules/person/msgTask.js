import { requestGet, requestPut } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'

/**
 * 查询消息任务详情
 */
export function getById (id) {
  return requestGet(API_KEY.msgTask.def + id, {})
}
/**
 * 更新消息任务状态
 */
export function updateMsgStatus (params) {
  return requestPut(API_KEY.msgTask.updateMsgStatus, params, REQUEST_KEY.requestMode.formData)
}
