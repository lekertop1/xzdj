import { requestGet, requestPost } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'

export function getDeptSelect (orgId = '') {
  return requestGet(API_KEY.dept.listSelectDeptNames, { orgId })
}
export function exportDept (params) {
  return requestPost(API_KEY.dept.exportDept, params, REQUEST_KEY.requestMode.json)
}
