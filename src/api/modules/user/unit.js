import { requestGet, requestPost } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'

export function getUnitSelect () {
  return requestGet(API_KEY.unit.select, {})
}

export function exportUnit (params) {
  return requestPost(API_KEY.unit.exportUnit, params, REQUEST_KEY.requestMode.json)
}
