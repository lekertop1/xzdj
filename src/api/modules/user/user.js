import { requestGet, requestPost, requestPut } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'

export function updateUserStatus (userId, activeStatus) {
  return requestPut(`${API_KEY.user.activeUser}/${userId}`, { activeStatus }, REQUEST_KEY.requestMode.formData)
}

export function getUserById (id) {
  return requestGet(API_KEY.user.crudUser + id, {})
}

export function updateUser (params) {
  return requestPut(API_KEY.user.crudUser, params, REQUEST_KEY.requestMode.json)
}

export function createUser (params) {
  return requestPost(API_KEY.user.crudUser, params, REQUEST_KEY.requestMode.json)
}

export function resetPass (params) {
  return requestPut(`${API_KEY.user.resetPass}/${params}`, REQUEST_KEY.requestMode.formData)
}

/**
 * 导出用户
 */
export function exportUser (params) {
  return requestPost(API_KEY.user.exportUser, params, REQUEST_KEY.requestMode.json)
}

/**
 * 获得下载导入模板路径
 */
export function getExcelImportTemplate (fileName) {
  return requestGet(`${API_KEY.user.getExcelImportTemplate}?fileName=` + fileName)
}

/**
 * 注册
 */
export function register (params) {
  return requestPost(API_KEY.user.registerUser, params, REQUEST_KEY.requestMode.json)
}
/**
* 查询通讯录、单位部门用户下拉框
* @author Enzo
* @date 2020-8-10
*/
export function getListSelectOrgDeptUser (params) {
  return requestGet(API_KEY.user.listSelectOrgDeptUser, params)
}
export function listSelectDeptUser (params) {
  return requestGet(API_KEY.user.listSelectDeptUser, params)
}
export function listSelectUser (params) {
  return requestGet(API_KEY.user.listSelectUser, params)
}
export function defaultIdentity () {
  return requestPut(API_KEY.user.defaultIdentity, {})
}
