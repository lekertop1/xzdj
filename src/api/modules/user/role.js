import { requestGet, requestPost, requestPut } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'

/**
 * 获取我的权限信息
 */
export function getRoleMenu () {
  return requestGet(API_KEY.menu.findMyMenu, {})
}

export function getRoleById (id) {
  return requestGet(API_KEY.role.curdRole + id, {})
}

export function createRoleData (params) {
  return requestPost(API_KEY.role.curdRole, params, REQUEST_KEY.requestMode.json)
}

export function updateRoleData (params) {
  return requestPut(API_KEY.role.curdRole, params, REQUEST_KEY.requestMode.json)
}

export function getRoleSelect () {
  return requestGet(API_KEY.role.readRoleSelect, {})
}
