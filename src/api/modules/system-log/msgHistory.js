import { requestPost } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'
/**
 * 分页查询消息发送历史记录信息
 */
export function getMsgHistoty (params) {
  return requestPost(API_KEY.log.logMsgHistoryPage, params, REQUEST_KEY.requestMode.json)
}
