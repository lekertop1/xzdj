import { requestGet } from '../../../utils/request'
import API_KEY from '../../index'

/**
 * 获取最新版本信息
 * @returns {*}
 */
export function getNewVersion () {
  return requestGet(API_KEY.version.readNewVersion, {})
}
