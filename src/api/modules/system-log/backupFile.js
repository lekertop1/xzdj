import { requestPost } from '../../../utils/request'
import API_KEY from '../../index'

/**
 * 文件备份
 * @param {Object} params
 */
export function backupFile (params) {
  return requestPost(API_KEY.syslogBackupFile.def, params)
}
