import { requestPost } from '../../../utils/request'
import API_KEY from '../../index'
import REQUEST_KEY from '../../../config/request'

/**
 * 导出文件上传记录信息
 */
export function exportSyslogFileLog (params) {
  return requestPost(API_KEY.log.exportSyslogFileLog, params, REQUEST_KEY.requestMode.json)
}
