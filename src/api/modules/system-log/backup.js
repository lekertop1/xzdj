import { requestPost } from '../../../utils/request'
import API_KEY from '../../index'

/**
 * 数据备份
 * @param backupType
 * @returns {*}
 */
export function backupData (backupType) {
  return requestPost(API_KEY.log.curdBackup, { backupType })
}

/**
 * 数据还原
 * @param pk
 * @returns {*}
 */
export function backupRestore (pk) {
  return requestPost(API_KEY.log.reduceBackup + pk, {})
}
