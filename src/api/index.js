/**
 * 接口地址(所有接口地址都在这里书写，进行统一的管理)
 * @type {{}}
 */
import REQUEST_KEY from '../config/request'

const API_KEY = {
  // 登陆相关接口地址
  login: {
    // 登陆
    login: '/login',
    // 登出
    loginOut: '/loginOut',
    loginPhone: '/loginPhone',
    loginEmail: '/loginEmail',
    getRsaPublicKey: '/getRsaPublicKey',
    weChatLogin: '/weChatAuthentication/weChatQrLogin'
  },
  // 菜单相关接口地址
  menu: {
    findMyMenu: '/systemMenu/listMyMenu',
    readMenuByPage: '/systemMenu/page',
    crudMenu: '/systemMenu/',
    // 获取菜单下拉
    readMenuSelect: '/systemMenu/listSelectMenuNames'
  },
  dept: {
    readDeptByPage: '/accountDept/page',
    // 删除部门列表
    deleteDeptByKey: '/accountDept/',
    def: '/accountDept/',
    listSelectDeptNames: '/accountDept/listSelectDeptNames',
    // 导出信息
    exportDept: '/accountDept/export',
    // 模板导入信息
    importDept: '/accountDept/import',
    // 获得下载导入模板路径
    getExcelImportTemplate: '/logFile/getExcelImportTemplate'
  },
  user: {
    // 获取用户列表信息
    readUserByPage: '/accountUser/page',
    // 创建及更新用户信息
    crudUser: '/accountUser/',
    // 删除用户
    deleteUserByKey: '/accountUser/',
    activeUser: '/accountUser/activeUser',
    // 修改当前登录用户信息
    updateLoginUser: '/accountUser/updateLoginUser',
    // 重置密码
    resetPass: '/accountUser/resetPass',
    // 查询单位部门用信息
    getUnitDeptUserVo: '/accountUser/getUnitDeptUserVo/',
    // 获取当前登录用户信息
    getSysUser: '/accountUser/getMyUser/',
    // 修改当前登录用户的登录密码
    updateMyPass: '/accountUser/updateMyPass',
    // 导出用户
    exportUser: '/accountUser/exportUser',
    // 导入用户
    importUser: '/accountUser/importUser',
    // 获得下载导入模板路径
    getExcelImportTemplate: '/logFile/getExcelImportTemplate',
    // 注册
    registerUser: '/accountUser/registerUser',
    // 单位部门用户下拉框/通讯录
    listSelectOrgDeptUser: '/accountUser/listSelectOrgDeptUser',
    listSelectUser: '/accountUser/listSelectUser',
    listSelectDeptUser: '/accountUser/listSelectDeptUser',
    defaultIdentity: '/accountUser/bindDefaultRole'
  },
  log: {
    readLoginLog: '/logLogin/page',
    readOperationLog: '/logOperation/page',
    readFileLog: '/logFile/page',
    uploadFileLog: '/logFile/upload/',
    exportSyslogFileLog: '/logFile/exportSyslogFileLog',
    readVersion: '/logVersion/page',
    deleteVersion: '/logVersion/',
    readBackup: '/logBackup/page/',
    importBackup: '/logBackup/importBackup/',
    curdBackup: '/logBackup/',
    reduceBackup: '/logBackup/restore/',
    logMsgHistoryPage: '/logMsgHistory/page'
  },
  role: {
    readRoleByPage: '/accountRole/page',
    curdRole: '/accountRole/',
    readRoleSelect: '/accountRole/listSelectRoleNames'
  },
  parameter: {
    readParameter: '/systemParameter/page',
    curdParameter: '/systemParameter/'
  },
  message: {
    readMessage: '/systemMsgTemplate/page',
    curdMessage: '/systemMsgTemplate/'
  },
  dictionary: {
    readDictionaryType: '/systemDictionary/listRootDictionary',
    readDataByPage: '/systemDictionary/page',
    insertRootDictionary: '/systemDictionary/insertRootDictionary',
    updateRootDictionary: '/systemDictionary/updateRootDictionary',
    deleteRootDictionary: '/systemDictionary/deleteRootDictionary/',
    curdDictionary: '/systemDictionary/',
    readDictionarySelect: '/systemDictionary/selectDictionaryName',
    readDictionarySelectByKey: '/systemDictionary/selectDictionaryNameByGroupKey',
    readDictionarySelectByKeys: '/systemDictionary/selectDictionaryNameInGroupKey',
    readDictionaryByRootCode: '/systemDictionary/listDictionaryByRootCode/'
  },
  version: {
    curdVersion: '/logVersion/',
    readVersionById: '/logVersion/',
    readNewVersion: '/logVersion/getNewVersion'
  },
  unit: {
    // 查询单位列表
    page: '/accountOrg/page',
    def: '/accountOrg/',
    // 查询单位名称下拉框
    select: '/accountOrg/listSelectOrgNames',
    // 导出信息
    exportUnit: '/accountOrg/export',
    // 导入信息
    importUnit: '/accountOrg/import'
  },
  fileLog: {
    createFileLog: '/logFile/upload',
    uploadWangEditorImg: '/syslogFileLog/uploadWangEditorImg/'

  },
  loginLog: {
    syslogLoginLog: '/logLogin/pageMyLogin'
  },
  syslogBackupFile: {
    // 文件备份
    def: '/logBackupFile/',
    page: '/logBackupFile/page',
    importBackup: REQUEST_KEY.requestParams.prefix + '/syslogBackupFile/importBackup/'
  },
  fileBasisHistoricalDown: {
    // 文件下载记录
    byFileLogId: '/fileBasisHistoricalDown/byFileLogId',
    page: '/fileBasisHistoricalDown/page/',
    getMyDownFileDateCount: '/fileBasisHistoricalDown/getMyDownFileDateCount',
    delete: 'fileBasisHistoricalDown/'
  },
  sendMsg: {
    sendMailVerificationCode: '/sendMsg/sendMailVerificationCode',
    sendPhoneVerificationCode: '/sendMsg/sendPhoneVerificationCode',
    // 邮箱找回，重置密码
    retrievePasswordByMail: '/accountUser/retrievePasswordByMail',
    updatePasswordByPhone: '/accountUser/retrievePasswordByPhone'
  },
  dingTalkAuthentication: {
    // 钉钉二维码登录
    dingTalkQrLogin: '/dingTalkAuthentication/dingTalkQrLogin',
    // 钉钉绑定
    bindDingTalk: '/dingTalkAuthentication/bindDingTalk',
    // 钉钉解绑
    unBindDingTalk: '/dingTalkAuthentication/unBindDingTalk',
    // 获取钉钉认证信息
    getDingTalkAuthentication: '/dingTalkAuthentication/getDingTalkAuthentication',
    readDingTalkAuthentication: '/dingTalkAuthentication/getDingTalkAuthentication'
  },
  weChatAuthentication: {
    readtWeChatAuthentication: '/weChatAuthentication/getWeChatAuthentication'
  },
  systemSsoCorpApp: {
    def: '/systemSsoCorpApp/',
    // 分页查询第三方应用授权信息
    page: '/systemSsoCorpApp/page'
  },
  systemParameter: {
    readParameter: '/systemParameter/getParameterByKey'
  },
  dcSso: {
    // 第三方单点登录
    getSsoTempAuthCode: '/dcSso/getSsoTempAuthCode',
    getSsoUserByConfigTempAuthCode: '/dcSso/getSsoUserByConfigTempAuthCode'
  },
  msgTask: {
    def: '/msgTask/',
    page: '/msgTask/page/',
    updateMsgStatus: '/msgTask/updateMsgStatus/'
  },
  msgInside: {
    def: '/msgInside/',
    page: '/msgInside/page/',
    pageMySend: '/msgInside/pageMySend/'
  }, // 农村党支部
  ruralPartyBranch: {
    page: '/ruralPartyBranch/page',
    curdRural: '/ruralPartyBranch',
    insert: '/ruralPartyBranch/',
    update: '/ruralPartyBranch/'
  }, // 农村详情
  ruralInfo: {
    page: '/ruralInfo/page',
    curdRural: '/ruralInfo',
    createData: '/ruralInfo/'
  },
  ruralGridDistribution: {
    page: '/ruralGridDistribution/page',
    curdRural: '/ruralGridDistribution',
    createData: '/ruralGridDistribution/',
    update: '/ruralGridDistribution/'
  },
  ruralGridStatistical: {
    page: '/ruralGridDStatistical/page',
    curdRural: '/ruralGridDStatistical',
    createData: '/ruralGridDStatistical/',
    update: '/ruralGridDStatistical/'
  }
}
export default API_KEY
