/*
*农村信息管理
* */
const RURALINFO = [{
  path: '/ruralInfo',
  name: 'ruralInfo',
  // 农村信息管理
  component: () => import('@/views/ruralInfos/ruralInfo/ruralInfo'),
  // meta，具体用法请参照vue-router文档
  meta: {
    // 是否需要登录访问
    isLogin: true,
    // 是否需要鉴权
    isAuth: true,
    btnAuthList: [],
    data: [{
      title: '农村详情',
      path: '/ruralInfo'
    }]
  }
}, {
  path: '/ruralPartyBranch',
  name: 'ruralPartyBranch',
  // 农村党支部管理
  component: () => import('@/views/ruralInfos/ruralPartyBranch/ruralPartyBranch'),
  // meta，具体用法请参照vue-router文档
  meta: {
    // 是否需要登录访问
    isLogin: true,
    // 是否需要鉴权
    isAuth: true,
    btnAuthList: [],
    data: [{
      title: '农村党支部',
      path: '/ruralPartyBranch'
    }]
  }
},
{
  path: '/ruralGridDistribution',
  name: 'ruralGridDistribution',
  // 农村网格分布详情
  component: () => import('@/views/ruralInfos/ruralGridDistribution/ruralGridDistribution'),
  // meta，具体用法请参照vue-router文档
  meta: {
    // 是否需要登录访问
    isLogin: true,
    // 是否需要鉴权
    isAuth: true,
    btnAuthList: [],
    data: [{
      title: '农村网格分布',
      path: '/ruralGridDistribution'
    }]
  }
},
{
  path: '/ruralGridStatistical',
  name: 'ruralGridStatistical',
  // 农村网格分布详情
  component: () => import('@/views/ruralInfos/ruralGridStatistical/ruralGridStatistical'),
  // meta，具体用法请参照vue-router文档
  meta: {
    // 是否需要登录访问
    isLogin: true,
    // 是否需要鉴权
    isAuth: true,
    btnAuthList: [],
    data: [{
      title: '农村网格分布',
      path: '/ruralGridStatistical'
    }]
  }
}
]
export default RURALINFO
