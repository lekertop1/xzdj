/*
 * @Author: duanyunlong
 * @since: 2020-07-19 09:31:08
 * @lastTime: 2020-07-23 13:32:18
 * @LastAuthor: Do not edit
 * @FilePath: \dc_container\public\js\browser_identify.js
 * @message: ie浏览器判定
 */
function isIeBrowser () {
  var userAgent = navigator.userAgent
  var reg = /msie (\d+\.\d+)/i
  // 判断是否ie，ie11以下
  var isIE = reg.test(userAgent)
  // edge
  // var isEdge = userAgent.indexOf('Edge') > -1 && !isIE
  // ie11
  var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf('rv:11.0') > -1
  // ie11以下浏览器
  if (isIE) {
  } else if (isIE11) {
    console.log('11')
  }
}
window.onload = isIeBrowser
